package com.example.tuto.controller;

import com.example.tuto.entity.Producto;
import com.example.tuto.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductServiceController {
    @Autowired
    private ProductoService productoService;

    @RequestMapping(value = "/api/products")
    public ResponseEntity<Object> getProducts() {
        return new ResponseEntity<>(productoService.obtenerTodosProductos(), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/products", method = RequestMethod.POST)
    public ResponseEntity<Object> createProduct(@RequestBody Producto product) {
        productoService.guardar(product);
        return new ResponseEntity<>("Product is created successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/products/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody Producto product) {
        productoService.eliminar(Integer.valueOf(id));
        //update
        product.setId(Integer.valueOf(id));
        productoService.guardar(product);
        return new ResponseEntity<>("Product is updated successsfully", HttpStatus.OK);
    }

    @RequestMapping(value = "/api/products/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> delete(@PathVariable("id") String id) {
        productoService.eliminar(Integer.valueOf(id));
        return new ResponseEntity<>("Product is deleted successsfully", HttpStatus.OK);
    }

}
