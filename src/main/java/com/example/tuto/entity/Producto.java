package com.example.tuto.entity;

import com.example.tuto.validator.ProductoCodigo;
import com.example.tuto.validator.ProductoPrecioUnidadesMinimo;
import com.example.tuto.validator.groups.Crear;
import com.example.tuto.validator.groups.Editar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;

@ProductoPrecioUnidadesMinimo
@Entity
public class Producto {

    @Id
    @Null(groups = Crear.class)
    @NotNull(groups = Editar.class)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ProductoCodigo
    @Size(min = 3, max = 8)
    @Pattern(regexp = "[A-Z0-9]+")
    private String codigo;

    @NotEmpty
    private String nombre;

    @NotNull
    @Min(value = 2, groups = {Crear.class, Editar.class})
    @Max(100)
    private Double precio;

    @NotNull
    @Min(1)
    private Integer unidadesMinimas;

    public Producto() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getUnidadesMinimas() {
        return unidadesMinimas;
    }

    public void setUnidadesMinimas(Integer unidadesMinimas) {
        this.unidadesMinimas = unidadesMinimas;
    }

}