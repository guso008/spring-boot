package com.example.tuto.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ProductoCodigoValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ProductoCodigo {
    String message() default "{productoCodigoValido.mensajePorDefecto}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
