package com.example.tuto.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ProductoPrecioUnidadesMinimoValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ProductoPrecioUnidadesMinimo {
    String message() default "{productoPrecioUnidadesMinimo.mensajePorDefecto}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
